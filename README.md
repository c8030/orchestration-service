# Camunda Assignment

## Deployment Architecture
Camunda engine is deployed as springboot service which also act as rest service interface that internally triggers camunda workflows. Services such as inventory service and order service are deployed separately which acts as external workers.

## Deployment Steps
* Build Codex-Core (Microservice Platform)
* Build and Run Orchestreation-Service
* Build and Run Order Service
* Build and Run inventory Service

Access Camunda Dashboard at: http://localhost:8080
Access swagger for Orchestreation service at: http://localhost:8080/swagger-ui/ 


## Components Details
### Orchestreation-Service
Acts as Camunda Engine and have rest APIs to trigger workflows.
### Codex-Core
Microservices Platform which contains all common logic such as:
* Global Exception Handler
* Swagger Configuration
* Logback MDC for Camunda
* RequestContextHolder for Camunda
* Abstract Base Classes for DTOs and Entities: Automatic setting of audit fields.
* Multilingual and MultiTenant Support
### Inventory Service
A Spring boot based microservice that handles all inventory related tasks.
### Order Service
A Spring boot based microservice that handles all order related tasks.
