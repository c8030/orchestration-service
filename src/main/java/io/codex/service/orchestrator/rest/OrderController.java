package io.codex.service.orchestrator.rest;

import io.codex.service.orchestrator.manager.OrderWorkflowManager;
import io.codex.service.orderservice.dto.OrderDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/orders")
public class OrderController {

    private final OrderWorkflowManager orderWorkflowManager;

    @PostMapping("/v1")
    public ResponseEntity placeOrder(@RequestBody OrderDTO orderDTO){
        orderWorkflowManager.placeOrder(orderDTO);
        return ResponseEntity.noContent().build();
    }

}
