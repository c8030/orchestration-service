package io.codex.service.orchestrator.manager.impl;

import io.codex.service.orchestrator.manager.OrderWorkflowManager;
import io.codex.service.orderservice.dto.OrderDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.camunda.bpm.engine.variable.value.SerializationDataFormat;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
@Slf4j
public class OrderWorkflowManagerImpl implements OrderWorkflowManager {

    private static final String ORDER_WORKFLOW = "Order_Workflow";
    private final ProcessEngine processEngine;

    @Override
    public void placeOrder(OrderDTO orderDTO) {
        log.info("inside placeOrder() {}", orderDTO);

        VariableMap variables = Variables.createVariables();
        variables.putValue("payload", orderDTO);
        processEngine.getRuntimeService().startProcessInstanceByKey(ORDER_WORKFLOW, getBusinessKey(), variables);
    }

    private String getBusinessKey(){
        return UUID.randomUUID().toString();
    }

}
