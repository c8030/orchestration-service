package io.codex.service.orchestrator.manager;

import io.codex.service.orderservice.dto.OrderDTO;

public interface OrderWorkflowManager {

    void placeOrder(OrderDTO orderDTO);

}
