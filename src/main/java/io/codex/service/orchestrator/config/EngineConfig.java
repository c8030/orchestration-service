package io.codex.service.orchestrator.config;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.spring.boot.starter.configuration.Ordering;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordering.DEFAULT_ORDER + 1)
public class EngineConfig implements ProcessEnginePlugin {

    private static final String DEFAULT_SERIALIZATION_FORMAT = Variables.SerializationDataFormats.JAVA.getName();

    @Override
    public final void preInit(final ProcessEngineConfigurationImpl processEngineConfiguration) {
        processEngineConfiguration.setJavaSerializationFormatEnabled(true);
    }

    @Override
    public final void postInit(final ProcessEngineConfigurationImpl processEngineConfiguration) {
        // Empty implementation
    }

    @Override
    public final void postProcessEngineBuild(final ProcessEngine processEngine) {
        // Empty implementation
    }
}
